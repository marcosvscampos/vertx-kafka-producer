package com.stonebank.vertx;

import com.stonebank.vertx.verticles.KafkaVerticle;
import io.vertx.core.Vertx;

public class VertxApp {

    public static void main(String [] args){
        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(new KafkaVerticle());
    }
}
