package com.stonebank.vertx.kafka;

import io.vertx.core.Vertx;
import io.vertx.kafka.client.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaProducerConfig {

    public static KafkaProducer<String, String> configureProducer(Vertx vertx){
        Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.METADATA_MAX_AGE_CONFIG, 1000);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "myClientId");

        KafkaProducer<String, String> producer = KafkaProducer.create(vertx, props);
        return producer;
    }
}
