package com.stonebank.vertx.verticles;

import com.stonebank.vertx.kafka.KafkaProducerConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import io.vertx.kafka.client.producer.RecordMetadata;

import java.util.Arrays;
import java.util.List;

public class KafkaVerticle extends AbstractVerticle {

    KafkaProducer<String, String> producer;

    @Override
    public void start() {
        producer = KafkaProducerConfig.configureProducer(vertx);
        List<String> messages = Arrays.asList("bmw", "mercedes", "sauber", "renault");

        messages.stream().forEach(ms -> {
            KafkaProducerRecord<String, String> record = KafkaProducerRecord.create("stonebank-topic", "key-" + ms, ms);
            producer.write(record, done -> {
                if (done.succeeded()) {
                    RecordMetadata recordMetadata = done.result();
                    System.out.println("Message " + record.value() + " written on topic=" + recordMetadata.getTopic() +
                            ", partition=" + recordMetadata.getPartition() +
                            ", offset=" + recordMetadata.getOffset());

                    producer.close();
                } else if (done.failed()) {
                    System.out.println("Execution failed: " + done.cause().getMessage());
                }
            });
        });
    }
}
